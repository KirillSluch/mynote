package com.task.notebookmvc.services;

import com.task.notebookmvc.dto.NoteDto;

import java.util.List;

public interface NoteService {
    List<NoteDto> getAllNotes();
    NoteDto getNote(Long id);
    NoteDto addNote(NoteDto note);
    void removeNote(Long noteId);
    NoteDto update(Long noteId, NoteDto note);
}
