package com.task.notebookmvc.services.impl;


import com.task.notebookmvc.dto.NoteDto;
import com.task.notebookmvc.exceptions.NotFoundException;
import com.task.notebookmvc.models.Note;
import com.task.notebookmvc.repositories.NotesRepository;
import com.task.notebookmvc.services.NoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class NoteServiceImpl implements NoteService {

    private final NotesRepository notesRepository;

    @Override
    public List<NoteDto> getAllNotes() {
        List<Note> notes = notesRepository.findAllByStateOrderById(Note.State.ACTIVE);
        return NoteDto.from(notes);
    }

    @Override
    public NoteDto getNote(Long id) {
        Note note = getOrElseThrow(id);
        return NoteDto.from(note);
    }

    @Override
    public NoteDto addNote(NoteDto noteToSave) {
        Note note = Note.builder()
                .title(noteToSave.getTitle())
                .theme(noteToSave.getTheme())
                .text(noteToSave.getText())
                .state(Note.State.ACTIVE)
                .build();
        notesRepository.save(note);
        return NoteDto.from(note);
    }

    @Override
    public void removeNote(Long noteId) {
        Note note = getOrElseThrow(noteId);
        note.setState(Note.State.DELETED);
        notesRepository.save(note);
    }

    @Override
    public NoteDto update(Long noteId, NoteDto noteToUpdate) {
        Note note = getOrElseThrow(noteId);

        note.setText(noteToUpdate.getText());
        note.setTheme(noteToUpdate.getTheme());
        note.setText(noteToUpdate.getText());

        notesRepository.save(note);
        return NoteDto.from(note);
    }

    private Note getOrElseThrow(Long noteId) {
        Note note = notesRepository.findById(noteId)
                .orElseThrow(() -> new NotFoundException("Note with id <" + noteId + "> not found!"));
        if (note.getState().equals(Note.State.DELETED)) {
            throw new NotFoundException("Note with id <" + noteId + "> not found!");
        }
        return note;
    }
}
