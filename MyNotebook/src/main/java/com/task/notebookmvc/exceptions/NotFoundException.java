package com.task.notebookmvc.exceptions;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String mess) {
        super(mess);
    }
}
