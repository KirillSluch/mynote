package com.task.notebookmvc.controllers;

import com.task.notebookmvc.dto.ExceptionDto;
import com.task.notebookmvc.dto.NoteDto;
import com.task.notebookmvc.models.Note;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tags(value = {
        @Tag(name = "Notes")
})
public interface NotesApi {
    @Operation(summary = "Получение всех заметок")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список заметок на странице",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = List.class))
                    }
            )
    })
    @GetMapping
    ResponseEntity<List<NoteDto>> getAllNotes();

    @Operation(summary = "Добавление заметки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Добавленная заметка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = NoteDto.class))
                    }
            )
    })
    @PostMapping
    ResponseEntity<NoteDto> saveNote(@RequestBody NoteDto newNote);

    @Operation(summary = "Получение заметки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Донные о заметке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = NoteDto.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{id}")
    ResponseEntity<NoteDto> getNoteById(@Parameter(description = "идентификатор заметки", example = "5")
                                        @PathVariable Long id);

    @Operation(summary = "Обновление заметки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Обновленная заметка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = NoteDto.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PatchMapping("/{id}")
    ResponseEntity<NoteDto> updateNote(@Parameter(description = "идентификатор заметки", example = "5")
                                       @PathVariable Long id, @RequestBody NoteDto noteToUpdate);


    @Operation(summary = "Удаление заметки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Заметка удалена"),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteNote(@Parameter(description = "идентификатор пользователя", example = "8")
                                 @PathVariable Long id);
}
