package com.task.notebookmvc.controllers;

import com.task.notebookmvc.dto.NoteDto;
import com.task.notebookmvc.services.NoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notes")
@RequiredArgsConstructor
public class NoteController implements NotesApi{
    private final NoteService noteService;

    @Override
    public ResponseEntity<NoteDto> getNoteById(Long id) {
        return ResponseEntity.ok(noteService.getNote(id));
    }

    @Override
    public ResponseEntity<List<NoteDto>> getAllNotes() {
        return ResponseEntity.ok(noteService.getAllNotes());
    }

    @Override
    public ResponseEntity<NoteDto> saveNote(NoteDto newNote) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(noteService.addNote(newNote));
    }

    @Override
    public ResponseEntity<?> deleteNote(Long id) {
        noteService.removeNote(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<NoteDto> updateNote(Long id, NoteDto noteToUpdate) {
        NoteDto note = noteService.update(id, noteToUpdate);
        return ResponseEntity.accepted()
                .body(note);
    }

}
