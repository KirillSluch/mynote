package com.task.notebookmvc.repositories;

import com.task.notebookmvc.models.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotesRepository extends JpaRepository<Note, Long> {
    List<Note> findAllByStateOrderById(Note.State state);
}

