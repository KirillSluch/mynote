package com.task.notebookmvc.dto;

import com.task.notebookmvc.models.Note;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Builder
@Data
@Schema(description = "Заметка")
public class NoteDto {
    @Schema(description = "идентификатор заметки", example = "1")
    private Long id;

    @Schema(description = "заголовок заметки", example = "Продукты")
    private String title;

    @Schema(description = "тема заметки", example = "Домашние дела")
    private String theme;

    @Schema(description = "тескст заметки", example = "Купить хлеб и молоко")
    private String text;

    public static NoteDto from(Note note) {
        return NoteDto.builder()
                .id(note.getId())
                .title(note.getTitle())
                .theme(note.getTheme())
                .text(note.getText())
                .build();
    }

    public static List<NoteDto> from(List<Note> notes) {
        return notes.stream()
                .map(NoteDto::from)
                .collect(Collectors.toList());
    }

}
