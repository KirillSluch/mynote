function getNotes() {
    return fetch('/notes')
        .then((response) => {
            return response.json();
        })
        .then((notes) => {
            fillList(notes);
            console.log(notes)
        })
}

function fillList(notes) {
    const main = document.getElementById("notesList");
    main.innerHTML = '';
    const separator = document.createElement('br');

    for (let i = 0; i < notes.length; i++) {
        const id = notes[i].id

        const div =document.createElement('div');
        div.className = 'note-holder';

        const titleInput = document.createElement('input');
        titleInput.value=notes[i].title;
        titleInput.id= 'title'+id;
        div.append(titleInput);

        const themeInput = document.createElement('input');
        themeInput.value=notes[i].theme;
        themeInput.id='theme'+id;
        div.append(themeInput);

        const textAreaDiv = document.createElement('div');
        textAreaDiv.className = "text-area-div";

        const textInput = document.createElement('textarea');
        textInput.value=notes[i].text;
        textInput.id='text'+id;
        textInput.maxLength = 512;
        textInput.addEventListener('keyup', function () {
            let maxLength = $(this).attr('maxlength');
            if ($(this).val().length > maxLength) {
                $(this).val($(this).val().substring(0, maxLength));
            }
        })
        textAreaDiv.append(textInput);
        div.append(textAreaDiv);

        const updateButton = document.createElement('button');
        updateButton.innerHTML = 'Update note';
        updateButton.addEventListener('click', function () {
            updateNote(id,
                document.getElementById('title' + id).value,
                document.getElementById('theme' + id).value,
                document.getElementById('text' + id).value
            );
        });
        div.append(updateButton);

        const deleteButton = document.createElement('button');
        deleteButton.innerHTML = 'Delete note';
        deleteButton.addEventListener('click', function () {
            deleteNote(id);
        });
        div.append(deleteButton);

        main.append(div);
        main.append(separator);
    }
}

function addNote(title, theme, text) {
    let body = {
        title: title,
        theme: theme,
        text: text
    }

    fetch('/notes', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(r => {window.location.replace("/html/notes.html")})
}

function updateNote(id, title, theme, text) {
    let body = {
        title: title,
        theme: theme,
        text: text
    }

    fetch('/notes/' + id, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(r => {
        getNotes()
    })
}

function deleteNote(id) {

    fetch('/notes/' + id, {
        method: 'DELETE'
    }).then(r => {
        getNotes()
    })
}